﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneApp8
{
    public partial class MainPage : PhoneApplicationPage
    {

        string sonido;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void SonidoButton_Click(object sender, RoutedEventArgs e)
        {
            if (SonidoButton.Content.ToString().Equals("Sonido: ON"))
            {
                sonido = "OFF";
                SonidoButton.Content = "Sonido: " + sonido;
                MediaElementMain.Pause();
            }

            else
            {
                sonido = "ON";
                SonidoButton.Content = "Sonido: " + sonido;
                MediaElementMain.Play();

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri ("/Niveles.xaml", UriKind.Relative));
        }
    }
}