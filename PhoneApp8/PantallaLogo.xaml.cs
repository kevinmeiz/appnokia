﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;

namespace PhoneApp8
{
    public partial class PantallaLogo : PhoneApplicationPage
    {
        IsolatedStorageSettings settings;
        Logo logo;

        public PantallaLogo()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(PantallaLogo_Loaded);
        }

        void PantallaLogo_Loaded(object sender, RoutedEventArgs e)
        {
            settings = IsolatedStorageSettings.ApplicationSettings;
            logo = settings["eleccion"] as Logo;
            ImagenLogo.Source = logo.imagenLogo;
            if (logo.estadoResuelto)
            {
    
                NumeroCaracteres.Text = "Logo Resuelto: " + logo.imagenLogo.ToString();
            }
            else
            {
               

                NumeroCaracteres.Text = "Numero de caracteres: " + logo.nombreLogo.Length.ToString();

                CajaNombre.MaxLength = logo.nombreLogo.Length;

            }

          
        }

        private void BotonLogo_Click(object sender, RoutedEventArgs e)
        {

            if (logo.nombreLogo.Equals(CajaNombre.Text.ToLower()))
            {
                settings = IsolatedStorageSettings.ApplicationSettings;
                ObservableCollection<Logo> arrLogos = new ObservableCollection<Logo>();

                foreach (var logitos in arrLogos)
                {
                    if (logitos.nombreLogo.Equals(logo.nombreLogo))
                    {
                        logitos.estadoResuelto = true;

                        if (!settings.Contains("nivel1Stage1"))
                            settings.Add("nivel1Stage1", arrLogos);
                        else
                        {
                            settings.Remove("nivel1Stage1");
                            settings.Add("nivel1Stage1", arrLogos);
                        }
                        break;
                    }
                }
                    


                MessageBox.Show("¡¡Correcto!!");

                NavigationService.Navigate(new Uri("/Niveles.xaml", UriKind.Relative));
                
            }
            else
            {
                MessageBox.Show("Intenta de nuevo :(");
            }

        }
    }
}