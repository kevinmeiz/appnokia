﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneApp8
{
    public partial class Niveles : PhoneApplicationPage
    {
        public Niveles()
        {
            InitializeComponent();
        }

        private void Stage11Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=1", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=2", UriKind.Relative));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=3", UriKind.Relative));
        }

        private void Stage21Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=4", UriKind.Relative));
        }

        private void Stage22Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=5", UriKind.Relative));
        }

        private void Stage23Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=6", UriKind.Relative));
        }

        private void Stage31Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=7", UriKind.Relative));
        }

        private void Stage32Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=8", UriKind.Relative));
        }

        private void Stage33Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Stage11.xaml?Nivel=9", UriKind.Relative));
        }
    }
}