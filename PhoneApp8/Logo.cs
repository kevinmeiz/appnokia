﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;

namespace PhoneApp8
{
    public class Logo
    {

        public string nombreLogo { get; set; }

        public string nivel { get; set; }

        public ImageSource imagenLogo {get; set ; }

        public Boolean estadoResuelto {get; set ; }

        public Logo(string nombreLogo, string nivel, ImageSource imagenLogo, Boolean estadoResuelto)
        {
            this.nombreLogo = nombreLogo;
            this.nivel = nivel;
            this.imagenLogo = imagenLogo;
            this.estadoResuelto = estadoResuelto;
        }

    }
}
