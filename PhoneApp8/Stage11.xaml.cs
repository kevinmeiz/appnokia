﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;

namespace PhoneApp8
{
    public partial class Stage11 : PhoneApplicationPage
    {
        string nivel;
        ObservableCollection<Logo> logos;
        ObservableCollection<Logo> arrLogos;
        IsolatedStorageSettings settings;

        public Stage11()
        {
            InitializeComponent();

            logos = new ObservableCollection<Logo>();

            this.Loaded += new RoutedEventHandler(Stage11_Loaded);
        }

        void Stage11_Loaded(object sender, RoutedEventArgs e)
        {
            settings = IsolatedStorageSettings.ApplicationSettings;
           
            if (NavigationContext.QueryString.ContainsKey("Nivel"))
            {
                nivel = NavigationContext.QueryString["Nivel"];
            }

            logos = new ObservableCollection<Logo>();

            if (nivel.Equals("1"))
            {
                if (settings.Contains("nivel1Stage1"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel1Stage1"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("sonic", "1", new BitmapImage(new Uri("/Images/Sonic.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("mario", "1", new BitmapImage(new Uri("/Images/Mario.png", UriKind.Relative)), false));
                    logos.Add(new Logo("call of duty", "1", new BitmapImage(new Uri("/Images/CallOfDuty.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("crash bandicoot", "1", new BitmapImage(new Uri("/Images/CrashBandicoot.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("halo", "1", new BitmapImage(new Uri("/Images/Halo.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("legend Of zelda", "1", new BitmapImage(new Uri("/Images/LegendOfZelda.png", UriKind.Relative)), false));
                    logos.Add(new Logo("tekken", "1", new BitmapImage(new Uri("/Images/Tekken.png", UriKind.Relative)), false));
                    logos.Add(new Logo("pac man", "1", new BitmapImage(new Uri("/Images/PacManpng.png", UriKind.Relative)), false));
                    logos.Add(new Logo("gran turismo", "1", new BitmapImage(new Uri("/Images/GranTurismo.png", UriKind.Relative)), false));

                    if (!settings.Contains("nivel1Stage1"))
                        settings.Add("nivel1Stage1", logos);
                    else
                    {
                        settings.Remove("nivel1Stage1");
                        settings.Add("nivel1Stage1", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }
                
            }

            if (nivel.Equals("2"))
            {
                if (settings.Contains("nivel1Stage2"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel1Stage2"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("metal gear solid", "2", new BitmapImage(new Uri("/Images/MetalGearSolid.png", UriKind.Relative)), false));
                    logos.Add(new Logo("ninja gaiden", "2", new BitmapImage(new Uri("/Images/NinjaGaiden.png", UriKind.Relative)), false));
                    logos.Add(new Logo("pokemon", "2", new BitmapImage(new Uri("/Images/Pokemon.png", UriKind.Relative)), false));
                    logos.Add(new Logo("super smash bros", "2", new BitmapImage(new Uri("/Images/SuperSmashBros.png", UriKind.Relative)), false));
                    logos.Add(new Logo("street fighter", "2", new BitmapImage(new Uri("/Images/StreetFighter.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("mario kart", "2", new BitmapImage(new Uri("/Images/MarioKart.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("need for speed", "2", new BitmapImage(new Uri("/Images/NeedForSpeed.png", UriKind.Relative)), false));
                    logos.Add(new Logo("yoshi", "2", new BitmapImage(new Uri("/Images/Yoshi.png", UriKind.Relative)), false));
                    logos.Add(new Logo("god of war", "2", new BitmapImage(new Uri("/Images/GodOfWar.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("gears of war", "2", new BitmapImage(new Uri("/Images/GearsOfWar.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel1Stage2"))
                        settings.Add("nivel1Stage2", logos);
                    else
                    {
                        settings.Remove("nivel1Stage2");
                        settings.Add("nivel1Stage2", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }
            }

            if (nivel.Equals("3"))
            {
                if (settings.Contains("nivel1Stage3"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel1Stage3"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("donkey kong", "1", new BitmapImage(new Uri("/Images/DonkeyKong.png", UriKind.Relative)), false));
                    logos.Add(new Logo("duck hunt", "1", new BitmapImage(new Uri("/Images/DuckHunt.png", UriKind.Relative)), false));
                    logos.Add(new Logo("fifa", "1", new BitmapImage(new Uri("/Images/Fifa.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("final fantasy", "1", new BitmapImage(new Uri("/Images/FinalFantasy.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("star fox", "1", new BitmapImage(new Uri("/Images/Fox.png", UriKind.Relative)), false));
                    logos.Add(new Logo("grand thef auto", "1", new BitmapImage(new Uri("/Images/GrandThefAuto.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("resident evil", "1", new BitmapImage(new Uri("/Images/Resident_Evil.png", UriKind.Relative)), false));
                    logos.Add(new Logo("silent hill", "1", new BitmapImage(new Uri("/Images/SilentHill.png", UriKind.Relative)), false));
                    logos.Add(new Logo("skyrim", "1", new BitmapImage(new Uri("/Images/Skyrim.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel1Stage3"))
                        settings.Add("nivel1Stage3", logos);
                    else
                    {
                        settings.Remove("nivel1Stage3");
                        settings.Add("nivel1Stage3", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }

            if (nivel.Equals("4"))
            {
                if (settings.Contains("nivel2Stage1"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel2Stage1"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("dantes inferno", "2", new BitmapImage(new Uri("/Images/210.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("spyro", "2", new BitmapImage(new Uri("/Images/211.png", UriKind.Relative)), false));
                    logos.Add(new Logo("guitar hero", "2", new BitmapImage(new Uri("/Images/212.png", UriKind.Relative)), false));
                    logos.Add(new Logo("just dance", "2", new BitmapImage(new Uri("/Images/213.png", UriKind.Relative)), false));
                    logos.Add(new Logo("angry birds", "2", new BitmapImage(new Uri("/Images/214.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("plants vs zombies", "2", new BitmapImage(new Uri("/Images/215.png", UriKind.Relative)), false));
                    logos.Add(new Logo("rayman", "2", new BitmapImage(new Uri("/Images/216.png", UriKind.Relative)), false));
                    logos.Add(new Logo("minecraft", "2", new BitmapImage(new Uri("/Images/217.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("mortal kombat", "2", new BitmapImage(new Uri("/Images/218.png", UriKind.Relative)), false));
                    logos.Add(new Logo("uncharted", "2", new BitmapImage(new Uri("/Images/219.png", UriKind.Relative)), false));

                    if (!settings.Contains("nivel2Stage1"))
                        settings.Add("nivel2Stage1", logos);
                    else
                    {
                        settings.Remove("nivel2Stage1");
                        settings.Add("nivel2Stage1", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

                
            }


            if (nivel.Equals("5"))
            {
                if (settings.Contains("nivel2Stage2"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel2Stage2"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("kirby", "2", new BitmapImage(new Uri("/Images/220.png", UriKind.Relative)), false));
                    logos.Add(new Logo("assasins creed", "2", new BitmapImage(new Uri("/Images/221.png", UriKind.Relative)), false));
                    logos.Add(new Logo("croc", "2", new BitmapImage(new Uri("/Images/222.png", UriKind.Relative)), false));
                    logos.Add(new Logo("contra", "2", new BitmapImage(new Uri("/Images/223.png", UriKind.Relative)), false));
                    logos.Add(new Logo("warcraft", "2", new BitmapImage(new Uri("/Images/224.png", UriKind.Relative)), false));
                    logos.Add(new Logo("megaman", "2", new BitmapImage(new Uri("/Images/225.png", UriKind.Relative)), false));
                    logos.Add(new Logo("metroid", "2", new BitmapImage(new Uri("/Images/226.png", UriKind.Relative)), false));
                    logos.Add(new Logo("rock band", "2", new BitmapImage(new Uri("/Images/227.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("league of legends", "2", new BitmapImage(new Uri("/Images/228.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("soul calibur", "2", new BitmapImage(new Uri("/Images/229.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel2Stage2"))
                        settings.Add("nivel2Stage2", logos);
                    else
                    {
                        settings.Remove("nivel2Stage2");
                        settings.Add("nivel2Stage2", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }

            if (nivel.Equals("6"))
            {
                if (settings.Contains("nivel2Stage3"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel2Stage3"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("devil may cry", "2", new BitmapImage(new Uri("/Images/230.png", UriKind.Relative)), false));
                    logos.Add(new Logo("dead space", "2", new BitmapImage(new Uri("/Images/231.png", UriKind.Relative)), false));
                    logos.Add(new Logo("half life", "2", new BitmapImage(new Uri("/Images/232.png", UriKind.Relative)), false));
                    logos.Add(new Logo("bomberman", "2", new BitmapImage(new Uri("/Images/233.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("doom", "2", new BitmapImage(new Uri("/Images/234.png", UriKind.Relative)), false));
                    logos.Add(new Logo("killzone", "2", new BitmapImage(new Uri("/Images/235.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("klonoa", "2", new BitmapImage(new Uri("/Images/236.png", UriKind.Relative)), false));
                    logos.Add(new Logo("mass effect", "2", new BitmapImage(new Uri("/Images/237.png", UriKind.Relative)), false));
                    logos.Add(new Logo("portal", "2", new BitmapImage(new Uri("/Images/238.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("turok", "2", new BitmapImage(new Uri("/Images/239.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel2Stage3"))
                        settings.Add("nivel2Stage3", logos);
                    else
                    {
                        settings.Remove("nivel2Stage3");
                        settings.Add("nivel2Stage3", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }


            if (nivel.Equals("7"))
            {
                if (settings.Contains("nivel3Stage1"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel3Stage1"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("age of empires", "2", new BitmapImage(new Uri("/Images/310.png", UriKind.Relative)), false));
                    logos.Add(new Logo("bayonetta", "2", new BitmapImage(new Uri("/Images/311.png", UriKind.Relative)), false));
                    logos.Add(new Logo("bioshock", "2", new BitmapImage(new Uri("/Images/312.png", UriKind.Relative)), false));
                    logos.Add(new Logo("king of fighters", "2", new BitmapImage(new Uri("/Images/313.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("castlevania", "2", new BitmapImage(new Uri("/Images/314.png", UriKind.Relative)), false));
                    logos.Add(new Logo("bad fur day", "2", new BitmapImage(new Uri("/Images/315.png", UriKind.Relative)), false));
                    logos.Add(new Logo("diablo", "2", new BitmapImage(new Uri("/Images/316.png", UriKind.Relative)), false));
                    logos.Add(new Logo("double dragon", "2", new BitmapImage(new Uri("/Images/317.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("manhunt", "2", new BitmapImage(new Uri("/Images/318.png", UriKind.Relative)), false));
                    logos.Add(new Logo("samurai shodown", "2", new BitmapImage(new Uri("/Images/319.png", UriKind.Relative)), false));

                    if (!settings.Contains("nivel3Stage1"))
                        settings.Add("nivel3Stage1", logos);
                    else
                    {
                        settings.Remove("nivel3Stage1");
                        settings.Add("nivel3Stage1", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }


            if (nivel.Equals("8"))
            {
                if (settings.Contains("nivel3Stage2"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel3Stage2"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("tenchu", "2", new BitmapImage(new Uri("/Images/320.png", UriKind.Relative)), false));
                    logos.Add(new Logo("soul reaver", "2", new BitmapImage(new Uri("/Images/321.png", UriKind.Relative)), false));
                    logos.Add(new Logo("adventure island", "2", new BitmapImage(new Uri("/Images/322.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("dark souls", "2", new BitmapImage(new Uri("/Images/323.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("dragon quest", "2", new BitmapImage(new Uri("/Images/324.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("fallout", "2", new BitmapImage(new Uri("/Images/325.png", UriKind.Relative)), false));
                    logos.Add(new Logo("starcraft", "2", new BitmapImage(new Uri("/Images/326.png", UriKind.Relative)), false));
                    logos.Add(new Logo("wolfestein", "2", new BitmapImage(new Uri("/Images/327.png", UriKind.Relative)), false));
                    logos.Add(new Logo("omega boost", "2", new BitmapImage(new Uri("/Images/328.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("tales of symphonia", "2", new BitmapImage(new Uri("/Images/329.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel3Stage2"))
                        settings.Add("nivel3Stage2", logos);
                    else
                    {
                        settings.Remove("nivel3Stage2");
                        settings.Add("nivel3Stage2", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }


            if (nivel.Equals("9"))
            {
                if (settings.Contains("nivel3Stage3"))
                {
                    arrLogos = arrLogos = new ObservableCollection<Logo>();
                    arrLogos = settings["nivel3Stage3"] as ObservableCollection<Logo>;


                    foreach (var logito in arrLogos)
                    {
                        if (logito.estadoResuelto)
                        {




                        }

                    }

                    ListLogos.ItemsSource = arrLogos;

                }
                else
                {
                    logos.Add(new Logo("thrill kill", "2", new BitmapImage(new Uri("/Images/330.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("xenoblade", "2", new BitmapImage(new Uri("/Images/331.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("okami", "2", new BitmapImage(new Uri("/Images/332.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("perfect dark", "2", new BitmapImage(new Uri("/Images/333.png", UriKind.Relative)), false));
                    logos.Add(new Logo("ace combat", "2", new BitmapImage(new Uri("/Images/334.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("ape escape", "2", new BitmapImage(new Uri("/Images/335.png", UriKind.Relative)), false));
                    logos.Add(new Logo("blood omen", "2", new BitmapImage(new Uri("/Images/336.jpg", UriKind.Relative)), false));
                    logos.Add(new Logo("chrono trigger", "2", new BitmapImage(new Uri("/Images/337.png", UriKind.Relative)), false));
                    logos.Add(new Logo("galaga", "2", new BitmapImage(new Uri("/Images/338.png", UriKind.Relative)), false));
                    logos.Add(new Logo("gekido", "2", new BitmapImage(new Uri("/Images/339.jpg", UriKind.Relative)), false));

                    if (!settings.Contains("nivel3Stage3"))
                        settings.Add("nivel3Stage3", logos);
                    else
                    {
                        settings.Remove("nivel3Stage3");
                        settings.Add("nivel3Stage3", logos);
                    }

                    ListLogos.ItemsSource = logos;
                }

            }

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
        
        }

        private void ListLogos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            settings = IsolatedStorageSettings.ApplicationSettings;
            Logo seleccion = ListLogos.SelectedItem as Logo;
            
            if (seleccion != null)
            {
                if (!settings.Contains("eleccion"))
                {
                    settings.Add("eleccion", seleccion);
                }
                else
                {
                    settings.Remove("eleccion");
                    settings.Add("eleccion", seleccion);
                }

                NavigationService.Navigate(new Uri("/PantallaLogo.xaml", UriKind.Relative));
            }
        }
    }
}